﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AvivaFizzBuzzNeilAllen.Startup))]
namespace AvivaFizzBuzzNeilAllen
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
