﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AvivaFizzBuzzNeilAllen.Models
{
    public class FizzBuzz
    {
        [Range(1, 1000, ErrorMessage = "Please use values between 1 to 1000")]
        public int BasicValue { get; set; }
        public string[] FizzBuzzList { get; set; }
    }
}