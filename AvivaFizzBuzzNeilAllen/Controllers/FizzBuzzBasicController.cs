﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AvivaFizzBuzzNeilAllen.Models;

namespace AvivaFizzBuzzNeilAllen.Controllers
{
    public class FizzBuzzBasicController : Controller
    {
        // GET: FizzBuzzBasic
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FizzBuzzList(FizzBuzz model)
        {
            if (ModelState.IsValid)
            {
                string Fizz = "fizz";
                string Buzz = "buzz";

                if (DateTime.Now.DayOfWeek.ToString() == "Wednesday")
                {
                    Fizz = "wizz";
                    Buzz = "wuzz";
                }

                model.FizzBuzzList = new string[model.BasicValue];
                
                for (int i = 1; i <= model.BasicValue; i++)
                {
                    if (i % 3 == 0 && i % 5 == 0)
                    {
                        model.FizzBuzzList[i - 1] = Fizz + Buzz;
                    }
                    else if (i % 3 == 0)
                    {
                        model.FizzBuzzList[i - 1] = Fizz;
                    }
                    else if (i % 5 == 0)
                    {
                        model.FizzBuzzList[i - 1] = Buzz;
                    }
                    else
                    {
                        model.FizzBuzzList[i - 1] = i.ToString();
                    }
                }
                return View(model);
            }
            return RedirectToAction("Index");                                  
        }
    }
}